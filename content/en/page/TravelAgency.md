---
title: "Travel Agency Project"

description: "Plan a fictional Vacation to a real place."
#cascade:
#  featured_image: '/images/gohugo-default-sample-hero-image.jpg'
---

# About
In the Travel Agency Project, we were asked to plan a Vacation with our group, as with we were actually going to go in a few days.

## Our Presentation

<iframe src="https://docs.google.com/presentation/d/17oej6KAWrz10iCOagwBWI_-MFMzBiqnU2O60OTYMzq8/edit" width="800" height="600">
